﻿using Task1.Classes;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Введіть строку:");
        string str = Console.ReadLine();
        Console.WriteLine("Введіть символ:");
        char symbol = Convert.ToChar(Console.ReadLine());
        if (str != null)
        {
            Console.WriteLine("Перевернута строка: {0}",str.Invert());
            Console.WriteLine("Кількість входжень даного символу: {0}", str.SymbolCount(symbol));
        }
        string[] arr = { "1" , "2" , "3" , "2" };
        Console.WriteLine("Введіть те що хочете знайти");
        string ch = Console.ReadLine();
        if (ch != null)
        {
            Console.WriteLine("В масиві {0} таких елементів", arr.ValueCount(ch));
        }
        string[] arr2 = arr.ArrayUnic();
        Console.WriteLine("Масив без повторень");
        for (int i = 0; i < arr2.Length; i++)
        {
            Console.WriteLine(arr2[i]);
        } 

        var test = new ExtendedDictionary<String, int, String>();

        test.Add("test", 1, "point");
        test.Add("hf", 2, "hdkj");
        test["test1"] = new ExtendedDictionaryElement<int, string>(3, "poin");
        test["test2"] = new ExtendedDictionaryElement<int, string>(5, "poi");

        Console.WriteLine("Кількість елементів = {0}", test.Count());

        Console.WriteLine(test.ContainsKey("test"));
        Console.WriteLine(test.ContainsKey("sdfg"));

        Console.WriteLine(test.ContainsValue(1, "point"));
        Console.WriteLine(test.ContainsValue(3, "poin"));

        test.Remove("test");

        Console.WriteLine("Кількість елементів після видалення = {0}", test.Count());

        foreach(var item in test)
        {
            Console.WriteLine(item.Value);
        }
    }
}