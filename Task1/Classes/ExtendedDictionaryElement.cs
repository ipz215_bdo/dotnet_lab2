﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1.Classes
{
    public class ExtendedDictionaryElement<U, V>
    {
        public U firstValue { get; set; }
        public V secondValue { get; set; }

        public ExtendedDictionaryElement(U firstValue, V secondValue)
        {
            this.firstValue = firstValue;
            this.secondValue = secondValue;
        }

        public override int GetHashCode()
        {
            return (firstValue.GetHashCode() * 3) + (secondValue.GetHashCode() * 41);
        }

        public override bool Equals(object? obj)
        {
            if (obj == null) return false;
            if (!(obj is ExtendedDictionaryElement<U, V>)) return false;
            ExtendedDictionaryElement<U, V> exDicEl = (ExtendedDictionaryElement<U, V>)obj;
            if (this.firstValue.Equals(exDicEl.firstValue) && this.secondValue.Equals(exDicEl.secondValue)) return true;
            return base.Equals(obj);
        }

        public override string ToString()
        {
            return firstValue + ", " + secondValue;
        }

    }
}
