﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Task1.Classes
{
    public static class ArrayExtention
    {
        public static int ValueCount <T> (this T[] array,T value)
        {
            int count = 0;
            
            for(int i = 0; i < array.Length; i++)
            {
                if (array[i].Equals(value))
                {
                    count++;
                }
            }

            return count;
        }

        public static T[] ArrayUnic<T>(this T[] array)
        {
            /*var newList = new List<T>(array.Distinct());*/
            T[] newArray = array.Distinct().ToArray();
            return newArray;
        } 
    }
}
