﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Task1.Classes;

namespace Task1.Classes
{
    public class ExtendedDictionary<T, U, V>
    {
        public Dictionary<T, ExtendedDictionaryElement<U, V>> _elements;
        public int count {get; set;}

        public ExtendedDictionary()
        {
            _elements = new Dictionary<T, ExtendedDictionaryElement<U, V>>();
        }

        public void Add(T key, U firstValue, V secondValue)
        {
            count++;
            _elements.Add(key, new ExtendedDictionaryElement<U, V>(firstValue, secondValue));
            /*elements[key] = new ExtendedDictionaryElement<U, V>(firstValue, secondValue);*/
        }

        public ExtendedDictionaryElement<U, V> this[T key]
        {
            get {
                return _elements[key];
            }
            set
            {
                count++;
                _elements[key] = value;
            }
        }

        public bool ContainsKey(T key)
        {
            return _elements.ContainsKey(key);
        }
        
        public bool ContainsValue(U firstValue, V secondValue)
        {
            return _elements.ContainsValue(new ExtendedDictionaryElement<U, V>(firstValue, secondValue));
        }
       
        public void Remove(T key)
        {
            count--;
            _elements.Remove(key);
        }
        
        public int Count()
        {
            return _elements.Count();
            return count;
        }

        public Dictionary<T, ExtendedDictionaryElement<U, V>>.Enumerator GetEnumerator()
        {
            return _elements.GetEnumerator();
        }

    }
}

