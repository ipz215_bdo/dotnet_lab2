﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1.Classes
{
    public static class StringExtention
    {
        public static string Invert(this string str)
        {
            string InvertStr = "";
            for (int i = str.Length - 1; i >= 0; i--)
            {
                InvertStr += str[i];
            }
            return InvertStr;
        }

        public static int SymbolCount(this string str, char _symbol)
        {
            int count = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == _symbol)
                {
                    count++;
                }
            }
            return count;
        }
    }
}
